import numpy as np
la = np.array([1,1,1,3,3,4,3,4,5,6,5,10,7,8,7,12])
lb = np.array([3,3,3,5,5,6,5,6,7,7,7,11,9,11,9,13])
temp1 = np.dot(la-la.mean(),lb-lb.mean())
temp2 = np.dot(la-la.mean(),la-la.mean())
beta = temp1 / temp2
arpha = lb.mean()-beta*la.mean()
lb_after = [int(i+0.5) for i in lb-arpha]
print(lb_after)