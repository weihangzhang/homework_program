import numpy as np 

#均衡化函数
def histequal(sk,smin,l):
    temp = int(((sk-smin)/(1-smin))*(l-1)+0.5)
    return temp



sk = np.array([0.09,0.23,0.48,0.66,0.78,0.86,0.95,1.00])  #累计直方图分布
smin = sk.min()
l = len(sk)
output_hist = []

for i in sk:
    temp = histequal(i,smin,l)
    output_hist.append(temp)

print(output_hist)


