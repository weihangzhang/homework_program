import numpy as np 
import scipy.io as sio
import numpy as np 
import matplotlib.pyplot as plt

#读入BSQ格式数据
data = sio.loadmat('matlab.mat')
data = data['a']


#划分数据
data_bsq = data.reshape(6,1024,1024)
image = data_bsq[5,:,:]

#显示数据
# print(image.shape)
plt.figure(1)
plt.subplot(1,2,1)
plt.imshow(image,cmap = 'gray')
plt.title('BSQ')


#转化为BIP格式
#data_bip = data 数组指向同一个空间 会出错！！！！！
data_bip = np.ones_like(data)
k = 0
for i in np.arange(1024*1024):
    for j in range(6):
        data_bip[k] = data[i+1024*1024*j]
        k += 1
image_bip = data_bip.reshape(1024,1024,6)
print('BIP格式图片前六位')
print(data_bip[:6])
print('***************')
print('BSQ格式图片前六位')
print(data_bsq[:,0,0])
#显示图片
plt.subplot(1,2,2)
plt.imshow(image_bip[:,:,5],cmap='gray')
plt.title('BIP')
plt.show()